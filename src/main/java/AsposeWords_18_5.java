import javassist.ClassPool;
import javassist.CtClass;

public class AsposeWords_18_5 {
    public static void main(String[] args) throws Exception {
        String fullPath = "/Users/liuzy/Downloads/aspose-words-18.5-jdk16.jar";
        ClassPool.getDefault().insertClassPath(fullPath);
        CtClass clazz = ClassPool.getDefault().getCtClass("com.aspose.words.zzZM4");
        clazz.getDeclaredMethod("zzZKn").setBody("{return 1;}");
        clazz.getDeclaredMethod("zzZKm").setBody("{return 1;}");
        clazz.writeFile();
    }
}
